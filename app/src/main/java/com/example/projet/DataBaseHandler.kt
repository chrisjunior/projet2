package com.example.projet

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.widget.Toast

val DATABASE_NAME="projet"
val TABLE_NAME="try"
val COl_NAME="nom"
val COL_AGE="age"
val COL_ID="id"

class DataBaseHandler(context: Context): SQLiteOpenHelper(context,DATABASE_NAME,null,){
    override fun onCreate(p0: SQLiteDatabase?) {
        val createTable = "CREATE TABLE" + TABLE_NAME¨+ "("+
        COL_ID+"INTEGER PRIMARY KEY AUTOINCREMENT"+ COl_NAME+"VARCHAR(256)"+
                COL_AGE+"INTEGER)";
        p0?.execSQL(createTable)
    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
  fun inserData(user: utilisateur){
      val db= this.writableDatabase
      var cv = ContentValues()
      cv.put(COl_NAME,user.nom)
      cv.put(COL_AGE,user.age)
      var result = db.insert(TABLE_NAME,null,cv)
      if(result == -1.toLong())
          Toast.makeText(context,"Failed",Toast.LENGTH_SHORT).Show()
  }
}